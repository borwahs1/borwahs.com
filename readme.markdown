#borwahs.com blog

Repository for [my](http://borwahs.com/) Jekyll based blog.

##Rakefile

The Rakefile allows you to perform some common tasks while using your Jekyll blog and pushing it to a remote server via `rsync` (Unix command).

###Tasks

The following tasks are defined in the Rakefile:

`rake build` will build one time using the Jekyll command without a local webserver or auto-regeneration of files.

`rake preview` will build using the Jekyll command and serve the blog locally at `http://localhost:4000/`.
    
`rake deploy` will `rsync` the built blog to the remote server with the configuration options defined in the YAML settings.

The `rake preview` is set as the default rake task.
    
###Deploy Config Settings

The `rsync` command is being used to push the site to a remote server. The following options will be used when running the `rsync` command:

    deploy:
      source: _site/
      user: usertodeploy
      host: hostname.com
      destination: ~/dest/folder/for/source/

`source` will default to `_site/` if no value is set.

The `rsync` command format with options:

    rsync -avzt --delete #{source} #{user}@#{host}:#{destination}

##Symbolset

This blog is using the [Symbolset](http://symbolset.com/) social font set. 

However, the required Symbolset files are not included in this repository. 

##Open Source Usage

The following is a list of open-source components used:

* [Gridism](http://cobyism.com/gridism/)

#License
The following directories and their contents are copyright © Rob Shaw:

* _posts/
* _projects/

Everything else within this repository is MIT Licensed. Please read the included LICENSE file.