module Jekyll

  class TagsPage < Page
    def initialize(site, base, dir, tags)
      @site = site
      @base = base
      @dir = dir
      @name = 'index.html'

      self.process(@name)
      self.read_yaml(File.join(base, '_layouts'), 'tags.html')

      self.data['tags'] = tags

      tags_page_title = site.config['tags']['tags_page_title'] || 'Tags'
      self.data['title'] = "#{tags_page_title}"
      self.data['header'] = "Rob Shaw"
    end
  end

  class TagPage < Page
    def initialize(site, base, dir, tag)
      @site = site
      @base = base
      @dir = dir
      @name = 'index.html'

      self.process(@name)
      self.read_yaml(File.join(base, '_layouts'), 'tag.html')
      self.data['tag'] = tag

      tag_page_title = site.config['tags']['tag_page_title'] || 'Tag: '
      self.data['title'] = "#{tag_page_title} #{tag}"
      self.data['header'] = "Rob Shaw"
    end
  end

  class TagPageGenerator < Generator
    safe true

    def generate(site)
      if site.layouts.key? 'tag'
        dir = site.config['tags']['tags_directory'] || 'tags'

        site.pages << TagsPage.new(site, site.source, dir, site.tags.keys)

        site.tags.keys.each do |tag|
          site.pages << TagPage.new(site, site.source, File.join(dir, tag), tag)
        end
      end
    end
  end

end