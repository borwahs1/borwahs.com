require 'yaml'

config = YAML.load_file("_config.yml")

task :default => :preview

def jekyll(opts="")
  sh "jekyll " + opts
end

# rsync the source directory to host:destination
def rsync(source, user, host, destination, exclude_list)
  exclude_file_options = ""

  exclude_list.each do |file|
    exclude_file_options << " --exclude '#{file}' "
  end

  sh "rsync -avzt #{exclude_file_options} --delete #{source} #{user}@#{host}:#{destination}"
end

# build changes
desc "build blog"
task :build do
  jekyll "build"
end

# serve the jekyll blog locally on port 4000
desc "build blog and serve locally"
task :preview do
  jekyll "serve --watch"
end

# deploy to server
desc "Deploy"
task :deploy => :build do
  puts "Attempting to deploy to server..."

  source       = config["deploy"]["source"]
  user         = config["deploy"]["user"]
  destination  = config["deploy"]["destination"]
  host         = config["deploy"]["host"]
  exclude_list = config["deploy"]["exclude_list"]

  validated = true

  if source.nil? or source.empty?
    source = "_site/"
  end

  if user.nil? or user.empty?
    puts "User must be defined."
    validated = false
  end

  if destination.nil? or destination.empty?
    puts "Destination must be defined."
    validated = false
  end

  if host.nil? or host.empty?
    puts "Host must be defined."
    validated = false
  end

  if !validated
    puts "ERROR: Could not deploy"
    next #use return in a method
  end

  rsync(source, user, host, destination, exclude_list)
end
